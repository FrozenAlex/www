'use strict';

// Библиотека для хэширования 
let bcrypt = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
    var User = sequelize.define('User', {
        id: {
            type: DataTypes.INTEGER(10),
            primaryKey: true,
            autoIncrement:true
        },
        username: DataTypes.STRING,
        password: DataTypes.STRING
    }, {
        hooks: {
            // Хэшируем пароль перед созданием пользователя
            beforeCreate: async (model) => {
                let salt = await bcrypt.genSalt(10);
                let hash = await bcrypt.hash(model.password, salt);

                model.password = hash;
            }
        }
    })

    return User;
};