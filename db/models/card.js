'use strict'

// Модель
module.exports = (sequelize, DataTypes) => {
    const Card = sequelize.define('Card', {
        id: {
            type: DataTypes.STRING(10),
            primaryKey: true
        },
        card: DataTypes.TEXT, // Текст
        data: {
            type: DataTypes.TEXT,
            get: function () {
                return JSON.parse(this.getDataValue('data'));
            },
            set(val) {
                this.setDataValue('data', JSON.stringify(val));
            }
        }, // Обьект
        visits: DataTypes.BIGINT // Количество посещений
    }, {
        hooks: {
            beforeCreate: (card) => {
                card.id = makeid(10);
            }
        }
    })
    return Card;
};

// Создает случайную строку
function makeid(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}