var element = document.getElementById("background2");

// On mouse move
function mouseMove(e){

    var x = e.clientX
    var y = e.clientY;

    var width = window.innerWidth;
    var height = window.innerHeight;


    var ydiff = 22 // (-12;12px);
    var xdiff = 22 // (-12;12px);
    
    var ty = (ydiff*2*(y/height))-ydiff;
    var tx = (xdiff*2*(x/width))-xdiff;
    TweenMax.to(element,0.9, {backgroundPosition:  (tx) + 'px ' + (ty) + 'px'})
}

document.body.addEventListener("mousemove", mouseMove);