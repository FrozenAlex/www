import './sass/loader.scss';
import './sass/main.scss';

import Main from './pages/Main.vue';
import AnimationList from './pages/AnimationList.vue'

import Vue from 'vue';
import VueRouter from 'vue-router';
// import "babel-polyfill";

Vue.use(VueRouter);

let router = new VueRouter({
  mode: "history",
  linkActiveClass: null,
  linkExactActiveClass: "is-active",
  base: "/",
  routes: [
    { path: "/", component: AnimationList },
    { path: "/list", component: AnimationList }
  ]
});


new Vue({
  router,
  render: h => h(Main),
  created: function() {}
}).$mount("#content");
