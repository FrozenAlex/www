// Переменные
let animationFrame; // Анимация
let index;
let cardList = document.getElementById("cardList"); // Элемент - список карточек
let params;

// Редактор
let editor = {
  div: null,
  current: {
    id: null,
    properties: null,
    options: null // Определения свойств
  },
  frame: null,
  properties: {
    div: null
  },
  close: () => {
    document.body.removeChild(editor.div);
    editor.div = null;
    editor.current = {};
    editor.frame = null;
    editor.properties = {};
  },
  open: (card) => {
    openEditor(card);
  }
};

// Заставка
let splash = {
  element: document.getElementById("splash"),
  statusdiv: document.querySelector("#splashStatus"),
  hide: function () {
    splash.element.style.opacity = "0";
    setTimeout(() => {
      splash.element.style.display = "none"
    }, 300);
  },
  show: function () {
    splash.element.style.opacity = "1";
    splash.element.style.display = "flex";
  },
  status: function (text) {
    splash.statusdiv.innerHTML = text;
  }
}

splash.status('Инициализация');


// Получить часть url после #
function getUrlHash() {
  var url = window.location.href;
  var index = url.indexOf("#");
  if (index > -1) return url.substring(index + 1)
  else return null;
}

// Функция для получения всех параметров в URL
function getAllUrlParams(url) {
  // get query string from url (optional) or window
  let queryString = url ? url.split("?")[1] : window.location.search.slice(1);

  // we'll store the parameters here
  let obj = {};

  // if query string exists
  if (queryString) {
    // stuff after # is not part of query string, so get rid of it
    queryString = queryString.split("#")[0];

    // split our query string into its component parts
    let arr = queryString.split("&");

    for (let i = 0; i < arr.length; i++) {
      // separate the keys and the values
      let a = arr[i].split("=");

      // in case params look like: list[]=thing1&list[]=thing2
      let paramNum = undefined;
      let paramName = a[0].replace(/\[\d*\]/, function (v) {
        paramNum = v.slice(1, -1);
        return "";
      });

      // set parameter value (use 'true' if empty)
      let paramValue = typeof a[1] === "undefined" ? true : decodeURIComponent(a[1]);

      // (optional) keep case consistent
      paramName = paramName.toLowerCase();
      // paramValue = paramValue.toLowerCase(); // неа)

      // if parameter name already exists
      if (obj[paramName]) {
        // convert value to array (if still string)
        if (typeof obj[paramName] === "string") {
          obj[paramName] = [obj[paramName]];
        }
        // if no array index number specified...
        if (typeof paramNum === "undefined") {
          // put the value on the end of the array
          obj[paramName].push(paramValue);
        } else {
          // if array index number specified...
          // put the value at that index number
          obj[paramName][paramNum] = paramValue;
        }
      } else {
        // if param name doesn't exist yet, set it
        obj[paramName] = paramValue;
      }
    }
  }

  return obj;
}

function loadCardBrowser(cards) {
  Object.keys(cards).forEach(function (key) {
    var card = document.createElement("div");
    card.classList.add("card");
    card.setAttribute("data-id", key);
    card.innerHTML =
      `<iframe src="animation/${key}/${cards[key].assets[0]}" frameborder="0"></iframe>
        <div class="card-buttons">
          <div class="pull-left">${cards[key].name}</div> <div class="pull-right"><i class="fas fa-link"></i><i class="fas fa-pencil-alt"></i></div>
        </div>`
    

    // Запуск анимации после загрузки фрейма
    card.querySelector("iframe").onload = (e) => {
      if (e.target.contentWindow.Init != null) {
        e.target.contentWindow.Init();
      }
    }

    // Слушаем событие click
    card.addEventListener('click', function (e) {
      var key = e.target.getAttribute("data-id");
      openEditor(key);
    });
    cardList.appendChild(card); // добавление открытки в каталог
  });

}


// Открыть фрейм на весь экран
function createFullScreenFrame(card) {
  var animationFrame;
  if (animationFrame != null) {
    animationFrame.remove();
  }

  // Создание Iframe и загрузка в него открытки
  animationFrame = document.createElement("iframe");
  animationFrame.src = `animation/${card.id}/index.html`;
  animationFrame.classList.add("fullScreen"); 

  // Иницилизация фрейма после загрузки
  animationFrame.onload = (e) => {
    splash.status("Загрузка завершена. Приятного просмтора!")
    // Если функция инициализации существует, инициализировать
    if (e.target.contentWindow.Init != null) {
      e.target.contentWindow.Init(card);
    }
    splash.hide(); // Скрыть экран загрузки
  }
  document.body.appendChild(animationFrame);
}


function openEditor(cardId) {
  editor.current.id = cardId;
  var div = document.createElement("div");
  div.classList.add("card-editor");
  div.innerHTML =
    `
        <div class="toolbar">
            <a class="pull-left">
                ${index.cards[cardId].name}
            </a>
            <a class="pull-right">
                <i class="fas fa-window-close" onclick="closeEditor()"></i>
            </a>
        </div>
        <div class="editor-mid">
          <iframe class="editor-iframe" id="cardprev-${cardId}" src="animation/${cardId}/index.html" frameborder="0"></iframe>
          <div class="editor-props">
            <div class="prop-header">Свойства открытки</div>
            <div id="prop-form" class="loading">
              
            </div>
            <div class="prop-bottom">
              
            <div class="prop-header">Ссылки</div>
              Длинная  <br>
              
              <input  id="longLink" type="text"> <br>
              Короткая  <br>
              <input  id="shortLink" type="text">
              
            </div>
            <button class="button" onclick="shortUrl()">Получить короткую <i class="fa fa-link"></i></button>
            <div onclick="shareVK()" class="button btn-share"><i class="fab fa-vk"></i> Поделиться</div>
          </div>
        </div>
        `;
  document.body.appendChild(div);
  editor.div = div;

  editor.frame = document.getElementById(`cardprev-${cardId}`);
  var propertyEditor = document.querySelector("#prop-form");
  editor.frame.onload = (e) => {
    // Если функция инициализации существует, инициализировать (Или запустить анимацию)
    if (e.target.contentWindow.Init != null) {
      e.target.contentWindow.Init();
    }

    var properties = getProps(e.target);
    editor.current.options = properties;
    if (properties != null) {
      propertyEditor.innerHTML = getPropsForm(properties);
    }
    updateIframe();
  }
  editor.properties.div = propertyEditor;
}

function closeEditor() {
  editor.close();
}

function getProps(iframe) {
  return iframe.contentWindow.properties;
}

function getPropsForm(props) {
  var html = "";
  Object.keys(props).forEach(function (key) {
    if (props[key].type == "text") {
      if(props[key].multiline){
        html += `<label for="prop-${key}">${props[key].name || key}</label> <br>
        <textarea name="${key}" id="prop-${key}" onkeyup="updateIframe()">${props[key].default || ""}</textarea> <br>`;
      } else html += `<label for="prop-${key}">${props[key].name || key}</label>
      <input type="text" name="${key}" id="prop-${key}" onkeyup="updateIframe()" value="${props[key].default || "" }"> <br>`;
    }

    if (props[key].type == "css") {
      html += 
      `<label for="prop-${key}">${props[key].name || key}</label>
      <input type="color" name="${key}" id="prop-${key}"  oninput="updateIframe()" value="${props[key].default || "" }"> <br>`;
    }
  });
  return html;
}

// Получить список измененных 
function getProperties() {
  var props = {};
  var urlElement = document.querySelector("#image-url");
  var formElements = document.querySelectorAll("#prop-form [name]");
  for (var i = 0; i < formElements.length; i++) {
    var pname = formElements[i].getAttribute("name")
    var pvalue = formElements[i].value;
    // Если различия есть, то не включаем свойство в список
    if (editor.current.options[pname].default !== pvalue) props[pname] = formElements[i].value;
  }
  return props;
}

function updateIframe() {
  var properties = getProperties();
  editor.current.properties = properties;
  editor.frame.contentWindow.applyOptions(properties);
  var urlObj = document.querySelector("#longLink");
  var vkLink = document.querySelector('#vkLink');
  var url = getCardURL(editor.current.id, properties);
  urlObj.value = url;
}

function shortUrl() {
  var urlObj = document.querySelector("#shortLink");
  var url = getBaseUrl() + '#'; // Адрес с #
  shortenUrl(editor.current)
    .then((data) => {
      urlObj.value = url + data;
    })
}

function getCardURL(id, params) {
  var url = getBaseUrl() + '?';

  var array = [];
  var data = params;
  data.id = id;
  Object.keys(params).forEach(function (key) {
    array.push(`${key}=${encodeURIComponent(params[key])}`);
  });
  url += array.join('&');

  return url;
}

function shortenUrl(card) {
  return new Promise((resolve, reject) => {
    var data = card.properties;
    data.id = card.id;

    console.log(data);
    fetch(`card/`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      })
      .then(function (response) {
        return response.text()
      })
      .then(function (data) {
        console.log(data);
        resolve(data);
      })
      .catch((err) => {
        reject(err)
      })
  })
}

function getCardData(hashtag) {
  return new Promise((resolve, reject) => {
    fetch(`/card/${hashtag}`)
      .then(response => {
        if (response.status !== 200) {
          reject("Error")
          return;
        }
        response.json()
          .then(function (data) {
            resolve(data);
          });
      })
      .catch(err => {
        reject(err);
      });
  })
}

// Инициализация
function init() {
  var hashtag = getUrlHash();
  params = getAllUrlParams(window.location.href);

  if (hashtag != null) {
    splash.status('Нашли хештег, смотрим на него...');
    getCardData(hashtag)
      .then((data) => {
        splash.status("Открытка найдена. Загрузка...")
        createFullScreenFrame(data);
        console.info("Карточка. Открываем iframe и скрываем все остальное");
      })
      .catch((err) => {
        splash.status('Хештег не найден <br> <a href="/">Вы можете перейти к анимациям</a>');
      })
  } else
  if (params.id != null) { // Старый метод
    splash.status('Нет хештега, но в параметрах указана открытка. Загружаем...');
    createFullScreenFrame(params);
  } else {
    splash.status('Нет параметров. Загружаем индекс каталога.');
    // Загрузка всех карточек
    fetch("index.json")
      .then(response => {
        if (response.status !== 200) {
          console.log("Ошибка с индексом. Status Code: " + response.status);
          splash.status('Ошибка. Не получилось загрузить список открыток. <br> Попробуйте перезагрузить страницу.');
          return;
        }
        response.json()
          .then(function (data) {
            index = data;
            splash.status("Открываем каталог")
            loadCardBrowser(index.cards);
            splash.hide();
          });
      })
      .catch(err => {
        splash.status('Ошибка. Не получилось загрузить список открыток. <br> Попробуйте перезагрузить страницу.');
      });
  }
}

function getBaseUrl() {
  var loc = window.location;
  return `${loc.protocol}//${loc.host}${loc.pathname}`;
}

function shareVK(e){
  var newTab = window.open('', '_blank');
                
  var url = getBaseUrl() + '#'; // Адрес с #
  shortenUrl(editor.current)
    .then((data) => {
      newTab.location.href = `https://vk.com/share.php?url=${encodeURIComponent(url + data)}&title=${encodeURIComponent('Открытка')}&noparse=true`;
    })
}

// Запуск скрипта
init();