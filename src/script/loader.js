// Params
function Init(params) {
  // Если имеются заданные свойства, то применить их
  if (params != null) applyOptions(params);

  // "Хук" после инициализации (если есть)
  if (typeof afterInit == "function") afterInit();
}

// The thing that applies options
function applyOption(name, value) {
  var prop = properties[name];
  if (prop != null) {
    if (prop.type == "text") {
      var element = document.querySelector(prop.selector);
      if (element != null ) {
        var str = value;
        if(prop.multiline) str = str.replace(/(?:\r\n|\r|\n)/g, '<br>');
        element.innerHTML = str;
      }
    }

    if (prop.type == "css") {
      var elements = document.querySelectorAll(prop.selector);
      for (var i = 0; i < elements.length; i++) {
        elements[i].style = `${prop.property}: ${value}`;
      }
    }
  }
}

// Применить 
function applyOptions(params) {
  if ((properties != null) | (params != null)) {
    Object.keys(params).forEach(function(key) {
      applyOption(key, params[key]);
    });
  }
}

// Fallback для старых браузеров для анимации 
var requestAnimationFrame =
  window.requestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.msRequestAnimationFrame ||
  function(callback) {
    window.setTimeout(callback, 1000 / 60);
  };
window.requestAnimationFrame = requestAnimationFrame;

if ((window.self == window.top) & (typeof(afterInit) === "function")) afterInit();
