const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const {
    VueLoaderPlugin
} = require('vue-loader');
const BrotliPlugin = require('brotli-webpack-plugin');


module.exports = {
    mode: "production",
    entry: {
        app: ['./src/index.js']
    },
    plugins: [
        // new CleanWebpackPlugin(["build"]),
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
        new VueLoaderPlugin(),
        new BrotliPlugin({
            asset: '[path].br[query]',
            test: /\.(js|css|html|svg)$/,
            threshold: 10240,
            minRatio: 0.8
        })
    ],

    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, "build"),
        publicPath: ` / `,
    },
    resolve: {
        extensions: ['.js', '.svg']
    },
    module: {
        rules: [{
            test: /\.txt$/,
            use: 'raw-loader'
        }, {
            test: /\.(sa|sc|c)ss$/,
            use: [{
                    loader: MiniCssExtractPlugin.loader,
                    options: {
                        publicPath: './'
                    }
                },
                {
                    loader: "css-loader",
                    options: {
                        minimize: true
                    }
                }, {
                    loader: "sass-loader"
                }
            ]

        }, {
            test: /\.vue$/,
            use: 'vue-loader'
        }, {
            test: /\.svg$/,
            use: [{
                loader: 'html-loader',
                options: {
                    minimize: true
                }
            }]
        }, {
            test: /\.(ttf|eot|png|gif)(\?[\s\S]+)?$/,
            use: [{
                loader: 'file-loader',
                options: {
                    outputPath: 'assets/'
                }
            }]
        }, {
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: [{
                loader: 'babel-loader',
                options: {
                    "presets": ["@babel/preset-env"],
                    "plugins": ["transform-regenerator"]
                }
            }],
        }]
    }
};