let models = require('../db/models');
const fs = require('fs-extra');
const Router = require("koa-router");

module.exports = function (app) {

    // Создание роутера
    let router = new Router();
    router.get("/card", async (ctx,next) =>{
        let cards = await models.Card.findAll();
        ctx.body = cards;
    })

    router.get("/new", async (ctx,next) =>{
        let card = await models.Card.create({
            card:"test",
            data:{hehe:"lol"},
            visits: 0
        })
        
        ctx.body = card
    })

    router.get("/news", async (ctx,next) =>{
        let user = await models.User.create({
            username:"lol",
            password: "Assssss"
        })
    
        ctx.body = user
    })

    router.post("/card", async (ctx, next) => {
        return db.addURL(ctx.request.body)
            .then(async (id) => {
                console.info(`Открытка ${JSON.stringify(ctx.request.body)} добавлена как #${id}`);
                ctx.body = id;
                next();
            })
            .catch(async (err) => {
                ctx.response.status = 500;
                ctx.body = err;
                console.log(`Ошибка добавления карточки ${ctx.request.body}`)
                next();
            }) // Добавим URL

    });

    router.get("/card/:id", async (ctx, next) => {
        let card = await models.Card.findById(ctx.params.id);
        
        if (card) {
            ctx.body = card;
        }
    });

    app.use(router.routes());
    app.use(router.allowedMethods());
}