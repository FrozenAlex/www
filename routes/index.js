let models = require('../db/models');
const fs = require('fs-extra');
const Router = require("koa-router");
var path = require('path');
var basename = path.basename(__filename);

module.exports = async function (app) {
    // Создание роутеров 
    let router = new Router();

    let files = await fs.readdirSync(__dirname)
    let filtered = await files.filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    filtered.forEach(file => {
        var setupRouter = require(path.join(__dirname, file)); // 
        setupRouter(app);
    });
    router.get("/card", (ctx, next) => {
        ctx.body = {
            ass: "sss"
        }
    })

    app.use(router.routes());
    app.use(router.allowedMethods({

    }));
}