'use strict'
require('dotenv').config()

const Koa = require("koa");

const serve = require("koa-static");
const bodyParser = require("koa-bodyparser");
const compress = require('koa-compress')

const routes = require('./routes');
const app = new Koa();

let models = require('./db/models');

// Порт приложения
let port = process.env.PORT || 3000;

// Включаем gzip
app.use(compress({
  threshold: 200,
  flush: require('zlib').Z_SYNC_FLUSH
}))

// Отдаем статику
app.use(serve("build"));
app.use(serve("static"));

// Парсер тела запроса
app.use(bodyParser({
  urlencoded: true
}));


// Инициализация базы данных и запуск сервера
models.sequelize.sync().then(async function () {
  /**
   * Listen on provided port, on all network interfaces.
   */
  // Подключение роутеров
  await routes(app);

  // Начинаем слушать
  app.listen(port, function () {
    console.log('Express server listening on port ' + port);
  });
});